package com.mprs.ecommerce.user.pojo;

import lombok.Data;

@Data
public class EcommerceUser {

	private String firstName;
	private String lastName;
	private String email;
	private String mobileNo;
	private String city;
}
