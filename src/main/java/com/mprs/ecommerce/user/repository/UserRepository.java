package com.mprs.ecommerce.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mprs.ecommerce.user.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
