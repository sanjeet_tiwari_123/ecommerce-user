package com.mprs.ecommerce.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mprs.ecommerce.user.model.User;
import com.mprs.ecommerce.user.pojo.EcommerceUser;
import com.mprs.ecommerce.user.service.UserRegistrationService;

@RestController
@RequestMapping("/registration")
public class UserRegistrationController {

	@Autowired
	private UserRegistrationService userRegistrationService;
	
	@PostMapping
	public ResponseEntity<String> registerUser(@RequestBody EcommerceUser ecomUser){
		User user = userRegistrationService.registerUser(ecomUser);
		if(user != null)
		{
			return new ResponseEntity<String>("User registered successfully",HttpStatus.CREATED);
		}
		return null;
	}
}
