package com.mprs.ecommerce.user.service;

import com.mprs.ecommerce.user.model.User;
import com.mprs.ecommerce.user.pojo.EcommerceUser;

public interface UserRegistrationService {

	User registerUser(EcommerceUser ecomUser);

}
