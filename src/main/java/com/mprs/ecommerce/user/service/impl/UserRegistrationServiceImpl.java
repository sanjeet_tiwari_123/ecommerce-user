package com.mprs.ecommerce.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mprs.ecommerce.user.model.User;
import com.mprs.ecommerce.user.pojo.EcommerceUser;
import com.mprs.ecommerce.user.repository.UserRepository;
import com.mprs.ecommerce.user.service.UserRegistrationService;

@Service
public class UserRegistrationServiceImpl implements UserRegistrationService{

	@Autowired
	UserRepository userRepository;
	
	@Override
	public User registerUser(EcommerceUser ecomUser) {
		User user = new User();
		user.setFirstName(ecomUser.getFirstName());
		user.setLastName(ecomUser.getLastName());
		user.setMobileNo(ecomUser.getMobileNo());
		user.setEmail(ecomUser.getEmail());
		user.setCity(ecomUser.getCity());
		return userRepository.save(user);
	}

}
